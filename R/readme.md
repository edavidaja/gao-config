# R config

## personal package libraries in VDI

Place the `.Renviron` file in your "My Documents" folder. R will check this file every time it starts, and set the installation location for your packages to the path you specify. It's important that this path not be a Windows network file path (e.g. `\\prod\userdata\FR_Data`), but instead a drive that has been mapped to your session of Windows (e.g. `R:`, `S:`, `T:`, or `X:`). Avoid `C:`.

This file is also a good place to store secrets--API keys, passwords, or other credentials--that you may need to access with your R script. You can then refer to them in the script using `Sys.getenv()`, as in `Sys.getenv("API_KEY")`, which lets you safely refer to secrets in your R code without having them appear in the source.